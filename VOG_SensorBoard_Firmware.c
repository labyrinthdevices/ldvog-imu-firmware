#define ON              1
#define OFF             0
#define CANALS          3
#define LHRH            0
#define LARP            1
#define RALP            2
#define SENS_BUF_SIZE   14
#define SENS_VAL_SIZE   7

#include "msp430fr5738.h"

volatile static unsigned char I2C_Read_Mode = OFF;
volatile static unsigned char I2C_Read_Single = OFF;
volatile static const unsigned char MPU6050_ADDR = 0x68;
volatile unsigned char *PTxData;                     // Pointer to TX data
volatile unsigned char TXByteCtr;
volatile unsigned char *PRxData;                     // Pointer to RX data
volatile unsigned char RXByteCtr;
volatile unsigned char TxRegister = 0x43;
volatile unsigned char TxBuffer[5];       // Allocate 5 bytes of RAM
volatile unsigned char RxBuffer[20];       // Allocate 20 bytes of RAM
volatile signed int Sensor[SENS_VAL_SIZE];       
volatile signed int Sensor_Index[SENS_VAL_SIZE] = {6,5,4,3,2,1,0};

volatile unsigned char Dum_Reg[6] = {65,66,67,68,69,70};

volatile static unsigned char WhoAmI = 0;
volatile static unsigned char TestReg1 = 0;
volatile static unsigned char TestReg2 = 0;
volatile static unsigned char TestReg3 = 0;

volatile static unsigned int LastRequest = 0;
volatile static unsigned int CurrentRequest = 0;

volatile static unsigned int Sample_Period = 60000;
volatile static unsigned int Sample_Period_Count = 0;
volatile static unsigned char Read_Gyro_Flag = OFF;
volatile static unsigned char Read_Sensor_Flag = OFF;
volatile static unsigned char Send_Sensor_Flag = OFF;
volatile static unsigned char Sent_Sensor_Flag = OFF;

volatile static unsigned int Gyro_Count = 0;

volatile static unsigned char UART_Char = 0;

void MPU6050_Initialize(void);
void MPU6050_Test_Registers(void);
void Delay_Loop(void);
void Send_Sensor(void);

//  JTAG Password Protection
unsigned int *JTAG1 = 0;
unsigned int *JTAG2 = 0;
unsigned long *JTAGPWD = 0;

void main(void)
{
    WDTCTL = WDTPW + WDTHOLD;
    
    JTAG1 = (unsigned int *)0xFF80;          // JTAG signature 1
    JTAG2 = (unsigned int *)0xFF82;          // JTAG signature 2
    JTAGPWD = (unsigned long *)0xFF88;       // JTAG password location
   
    *JTAG1 = 0xAAAA;                          // Enable JTAG lock/unlock with pwd
    *JTAG2 = 0x0002;                          // Pwd length = 2 words
    *JTAGPWD = 0x11112222;                    // Pwd = 0x22221111
    
    CSCTL0_H = 0xA5;
    CSCTL1 |= DCOFSEL0 + DCOFSEL1;             // Set max. DCO setting
    CSCTL2 = SELA_1 + SELS_3 + SELM_3;        // set ACLK = VLO; MCLK = DCO
    //CSCTL2 = SELA_3 + SELS_3 + SELM_3;        // set ACLK = MCLK = DCO
    CSCTL3 = DIVA_0 + DIVS_0 + DIVM_0;        // set all dividers 

    //  --- Initialize Digital Pins ---  
    
    P1DIR &= ~BIT0;             // INT pin from MPU-6000, make input
    
    P1DIR |= BIT5;              // nCS pin of MPU-6000, enable I2C
    Delay_Loop();
    P1OUT &= ~BIT5;
    Delay_Loop();
    P1OUT |= BIT5;
    Delay_Loop();
    
    //  --- Initialize Digital Pins --- 
    
    // Configure Pins for I2C
    P1SEL1 |= BIT6 + BIT7;                      // Pin init

    UCB0CTLW0 |= UCSWRST;                       // put eUSCI_B in reset state
    
    UCB0CTLW0 |= UCMODE_3 + UCMST + UCSSEL_2;   // I2C master mode, SMCLk 
    UCB0BRW = 40;                               // Baudrate = SMCLK / 8
    UCB0I2CSA = MPU6050_ADDR;                   // I2C Slave Address
    UCB0CTLW0 &=~ UCSWRST;	                    // Clear reset register
    UCB0IE |= UCTXIE0 + UCNACKIE + UCRXIE + UCSTPIE;      // Transmit and NACK interrupt enable

    // We need this delay here, otherwise the I2C communication to the MPU-6000 freezes
    for (int i = 0; i < 20000; i++);
    
    __enable_interrupt();
    MPU6050_Initialize();
    MPU6050_Test_Registers();
    __disable_interrupt();
    
    // Configure UART pins
    P2SEL1 |= BIT0;
    P2SEL0 &= ~(BIT0);
    // Configure UART 0
    UCA0CTL1 |= UCSWRST; 
    UCA0CTL1 = UCSSEL_2;                      // Set SMCLK as UCBRCLK
    UCA0BR0 = 4;                              // 115200 baud
    UCA0BR1 = 0; 
    UCA0MCTLW |= 0x5551;                      // 
                                              // UCBRSx = 0x55, UCBRFx = 
    UCA0CTL1 &= ~UCSWRST;                     // release from reset
    
    __delay_cycles(9000000);
    __delay_cycles(9000000);
    __delay_cycles(9000000);
    __delay_cycles(9000000);
    __delay_cycles(9000000);
    __delay_cycles(9000000);
    
    P2DIR &= ~BIT1;                           // P2.1 Input ISR -- Trigger from Flea3
    P2IES |= BIT1;                            // P2.1 high/low edge
    P2IE = BIT1;                              // P2.1 interrupt enabled
    P2IFG &= ~BIT1;                           // P2.1 IFG cleared
    
    //TA0CCTL0 = CCIE;                          // TACCR0 interrupt enabled
    //TA0CCR0 = 500;
    //TA0CTL |= TASSEL_1 + ID_3 + MC_2;                 // SMCLK, continuous mode
    //TA0CTL |= ID1 + ID0 + TASSEL_2 + MC_2;                 // SMCLK, continuous mode
    
    __bis_SR_register(LPM0_bits + GIE);     // Enter LPM0, enable interrupts
    
    while(1)
    {
      if (Read_Gyro_Flag) {
        Read_Gyro_Flag = OFF;
        I2C_Read_Mode = ON;
        PRxData = &RxBuffer[0];
        RXByteCtr = SENS_BUF_SIZE;                          // Load RX byte counter
        UCB0CTL1 |= UCTR;
        TXByteCtr = 1;
        TxBuffer[0] = 0x3B;
        PTxData = &TxBuffer[0];                 // TX array start address
        Read_Sensor_Flag = ON;
        for (int i = 0; i < SENS_BUF_SIZE; i = i + 2) {
          Sensor[Sensor_Index[i >> 1]] = (((signed int)RxBuffer[i+1])) + ((signed int)(RxBuffer[i]) << 8);
        }
        UCB0CTL1 |= UCTXSTT;                    // I2C start condition
        __bis_SR_register(LPM0_bits + GIE);    
      }
      if (Send_Sensor_Flag) {
        Send_Sensor_Flag = OFF;
        Gyro_Count++;
        /*Sensor[0] = Gyro_Count;
        Sensor[1] = Gyro_Count;
        Sensor[2] = Gyro_Count;
        Sensor[3] = Gyro_Count;
        Sensor[4] = Gyro_Count;
        Sensor[5] = Gyro_Count;
        Sensor[6] = Gyro_Count;*/
        if (Gyro_Count > 32767) {
          Gyro_Count = 0;
        }
        Send_Sensor();
        Sent_Sensor_Flag = OFF;
        __bis_SR_register(LPM0_bits + GIE);
      }
    }
	
}

void MPU6050_Initialize(void)
{
    Delay_Loop();
    TXByteCtr = 2; I2C_Read_Mode = OFF;   
    TxBuffer[0] = 0x6B; TxBuffer[1] = 0x01;                // Power Management Register
    PTxData = &TxBuffer[0];                 // TX array start address
    UCB0CTL1 |= UCTR + UCTXSTT;             // I2C TX, start condition
    __bis_SR_register(LPM0_bits + GIE);     // Enter LPM0, enable interrupts
    __no_operation();                       // Remain in LPM0 until all data is TX'd
    Delay_Loop();
    //while (UCB0CTL1 & UCTXSTP);             // Ensure stop condition got sent
    
    TXByteCtr = 2; I2C_Read_Mode = OFF;
    TxBuffer[0] = 0x1B; TxBuffer[1] = 0x08;                // Gyro Range Register
    PTxData = &TxBuffer[0];                 // TX array start address
    UCB0CTL1 |= UCTR + UCTXSTT;             // I2C TX, start condition
    __bis_SR_register(LPM0_bits + GIE);     // Enter LPM0, enable interrupts
    __no_operation();                       // Remain in LPM0 until all data is TX'd
    Delay_Loop();
    //while (UCB0CTL1 & UCTXSTP);             // Ensure stop condition got sent
    
    TXByteCtr = 2; I2C_Read_Mode = OFF;
    TxBuffer[0] = 0x1C; TxBuffer[1] = 0x00;               // Lin Acc Range Register
    PTxData = &TxBuffer[0];                     // TX array start address
    UCB0CTL1 |= UCTR + UCTXSTT;             // I2C TX, start condition
    __bis_SR_register(LPM0_bits + GIE);     // Enter LPM0, enable interrupts
    __no_operation();                       // Remain in LPM0 until all data is TX'd
    Delay_Loop();
    //while (UCB0CTL1 & UCTXSTP);             // Ensure stop condition got sent
}

void Delay_Loop(void)
{
  for (int i = 0; i < 10000; i++);
}

void MPU6050_Test_Registers(void)
{
  I2C_Read_Mode = ON;
  I2C_Read_Single = OFF;
  
  RXByteCtr = 2;
  TXByteCtr = 1;
  TxRegister = 0x75;
  TxBuffer[0] = 0x75;
  PTxData = &TxBuffer[0];                 // TX array start address
  PRxData = &RxBuffer[0];
  //while (UCB0CTL1 & UCTXSTP);             // Ensure stop condition got sent
  UCB0CTL1 |= UCTR;
  UCB0CTL1 |= UCTXSTT;                    // I2C start condition
  __bis_SR_register(LPM0_bits + GIE);     // Enter LPM0, enable interrupts, Remain in LPM0 until all data is RX'd
  //while (UCB0CTL1 & UCTXSTP);             // Ensure stop condition got sent
  WhoAmI = RxBuffer[0];
  Delay_Loop();
  
  RXByteCtr = 2;
  TXByteCtr = 1;
  TxBuffer[0] = 0x6B;
  PTxData = &TxBuffer[0];                 // TX array start address
  PRxData = &RxBuffer[0];
  //while (UCB0CTL1 & UCTXSTP);             // Ensure stop condition got sent
  UCB0CTL1 |= UCTR;
  UCB0CTL1 |= UCTXSTT;                    // I2C start condition
  __bis_SR_register(LPM0_bits + GIE);     // Enter LPM0, enable interrupts, Remain in LPM0 until all data is RX'd
  //while (UCB0CTL1 & UCTXSTP);             // Ensure stop condition got sent
  TestReg1 = RxBuffer[0];
  Delay_Loop();
  
  RXByteCtr = 2;
  TXByteCtr = 1;
  TxBuffer[0] = 0x1B;
  PTxData = &TxBuffer[0];                 // TX array start address
  PRxData = &RxBuffer[0];
  //while (UCB0CTL1 & UCTXSTP);             // Ensure stop condition got sent
  UCB0CTL1 |= UCTR;
  UCB0CTL1 |= UCTXSTT;                    // I2C start condition
  __bis_SR_register(LPM0_bits + GIE);     // Enter LPM0, enable interrupts, Remain in LPM0 until all data is RX'd
  //while (UCB0CTL1 & UCTXSTP);             // Ensure stop condition got sent
  TestReg2 = RxBuffer[0];
  Delay_Loop();
  
  RXByteCtr = 2;
  TXByteCtr = 1;
  TxBuffer[0] = 0x1C;
  PTxData = &TxBuffer[0];                 // TX array start address
  PRxData = &RxBuffer[0];
  //while (UCB0CTL1 & UCTXSTP);             // Ensure stop condition got sent
  UCB0CTL1 |= UCTR;
  UCB0CTL1 |= UCTXSTT;                    // I2C start condition
  __bis_SR_register(LPM0_bits + GIE);     // Enter LPM0, enable interrupts, Remain in LPM0 until all data is RX'd
  //while (UCB0CTL1 & UCTXSTP);             // Ensure stop condition got sent
  TestReg3 = RxBuffer[0];
  Delay_Loop();
  
  __no_operation();                       // Set breakpoint >>here<< and
  
  I2C_Read_Mode = OFF;
  I2C_Read_Single = OFF;
}


	
#pragma vector = USCI_B0_VECTOR 
__interrupt void USCIB0_ISR(void) 
{	
  switch(__even_in_range(UCB0IV,0x1E)) 
  {
        case 0x00: break;                    // Vector 0: No interrupts break;
        case 0x02: 
          break;
        case 0x04:
          UCB0CTLW0 |= UCTXSTT;             //resend start if NACK
          break;                            // Vector 4: NACKIFG break;
        case 0x08:                          // Stop Flag
          if (Read_Sensor_Flag) {  
            Read_Sensor_Flag = OFF;
            Send_Sensor_Flag = ON;
            Sent_Sensor_Flag = ON;
            __bic_SR_register_on_exit(CPUOFF);
          }
          break;
        case 0x16:
          if (I2C_Read_Single) {
            
          }
          else {
            RXByteCtr--;                            // Decrement RX byte counter
            if (RXByteCtr)
            {
              *PRxData++ = UCB0RXBUF;               // Move RX data to address PRxData
              if (RXByteCtr == 1)                   // Only one byte left?
                UCB0CTL1 |= UCTXSTP;                // Generate I2C stop condition
            }
            else
            {
              *PRxData = UCB0RXBUF;                 // Move final RX data to PRxData
              __bic_SR_register_on_exit(LPM0_bits); // Exit active CPU
            }
          }
          break;
        case 0x18: 
          if (I2C_Read_Mode) {
            if (TXByteCtr)  {                          // Check TX byte counter
              UCB0TXBUF = *PTxData++;               // Load TX buffer
              TXByteCtr--;                          // Decrement TX byte counter
            }
            else  {
              UCB0CTL1 &= ~UCTR;
              UCB0CTL1 |= UCTXSTT;
            }
          }
          else {
            if (TXByteCtr)                          // Check TX byte counter
            {
              UCB0TXBUF = *PTxData++;               // Load TX buffer
              TXByteCtr--;                          // Decrement TX byte counter
            }
            else
            {
              UCB0CTL1 |= UCTXSTP;                  // I2C stop condition
              UCB0IFG &= ~UCTXIFG;                  // Clear USCI_B1 TX int flag
              __bic_SR_register_on_exit(LPM0_bits); // Exit LPM0
            }  
          }
          break;                            // Vector 26: TXIFG0 break;
        default: break;
  }
}

void Send_Sensor(void) {
  for (int i = 0; i < 7; i++) {
    while (!(UCA0IFG&UCTXIFG));
    UCA0TXBUF = (Sensor[i] & 0xFF00) >> 8;
    while (!(UCA0IFG&UCTXIFG));
    UCA0TXBUF = Sensor[i] & 0x00FF;
  }
  __no_operation();
}

// Port 2 interrupt service routine
#pragma vector=PORT2_VECTOR
__interrupt void Port_2(void)
{
  //CurrentRequest = TA0R;
  P2IFG &= ~BIT1;                         // Clear P2.1 IFG
  Read_Gyro_Flag = ON;
  if (Sent_Sensor_Flag) {
    __no_operation();
  }
  //LastRequest = CurrentRequest;
  __bic_SR_register_on_exit(CPUOFF);     // Exit LPM0 
}